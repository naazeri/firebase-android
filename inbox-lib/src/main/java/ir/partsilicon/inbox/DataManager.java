package ir.partsilicon.inbox;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Configuration;
import com.activeandroid.query.Delete;

import java.util.List;

/***
 * Created by reza on 6/18/17.
 ***/

class DataManager {
    public static void init(Context context) {
        Remember.init(context, "inbox." + context.getPackageName());

        Configuration.Builder configurationBuilder = new Configuration.Builder(context);
        configurationBuilder.setDatabaseName("Inbox");
        configurationBuilder.setDatabaseVersion(2);
        configurationBuilder.addModelClasses(Message.class);
        configurationBuilder.addModelClasses(Category.class);

        ActiveAndroid.initialize(configurationBuilder.create(), false);
//        ActiveAndroid.initialize(context);

//        Hawk.init(context)
//                .setEncryptionMethod(HawkBuilder.EncryptionMethod.NO_ENCRYPTION)
//                .setLogLevel(LogLevel.NONE)
//                .setStorage(HawkBuilder.newSharedPrefStorage(context))
//                .build();
    }

    public static void saveMessage(List<Message> data) {
        try {
            ActiveAndroid.beginTransaction();
            for (Message message : data) {
                long l = message.save();
                Log.d(Constant.TAG, "message saved: " + l);
            }
            ActiveAndroid.setTransactionSuccessful();
            ActiveAndroid.endTransaction();

//        if (doAsync) {
//            new Async("messages", data).execute();
//        } else {
//            Hawk.put("messages", data);
//        }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void saveCategory(List<Category> data) {
        ActiveAndroid.beginTransaction();
        new Delete().from(Category.class).execute();
        for (Category category : data) {
            category.save();
        }
        ActiveAndroid.setTransactionSuccessful();
        ActiveAndroid.endTransaction();
    }

    public static void updateMessage(Message data) {
        // TODO: 6/22/17 update message here
    }

    public static void saveServerUrl(String url) {
        Remember.putString("url", url);
    }

    @NonNull
    public static String getServerUrl() {
        return Remember.getString("url", "");
    }

    public static void setFirstOpen(boolean b) {
        Remember.putBoolean("f", b);
    }

    public static boolean isFirstOpen() {
        return Remember.getBoolean("f", true);
    }

    public static void savePackageName(Context context) {
        Remember.putString("pa", context.getPackageName());
    }

    @NonNull
    public static String getPackageName() {
        return Remember.getString("pa", "");
    }

//    private static class Async extends AsyncTask<Void, Void, Boolean> {
//
//        String key;
//        Object value;
//
//        public <T> Async(String key, T value) {
//            this.key = key;
//            this.value = value;
//        }
//
//        @Override
//        protected Boolean doInBackground(Void... params) {
//            return Hawk.put(key, value);
//        }
//    }
}
