package ir.partsilicon.inbox;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.Display;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.squareup.picasso.Picasso;

import java.io.IOException;

public class InboxDetailActivity extends BaseActivity {

    public static final String MESSAGE = "m";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inbox_detail);

        try {
            TextView title = (TextView) findViewById(R.id.detail_title);
            TextView content = (TextView) findViewById(R.id.detail_content);
            final ImageView imageView = (ImageView) findViewById(R.id.detail_image);

            //enable html tags support
            final Message message = getIntent().getParcelableExtra(MESSAGE);
            title.setText(message.getTitle());
            content.setText(Html.fromHtml(message.getContent()));
            content.setMovementMethod(LinkMovementMethod.getInstance());

            Typeface font = Typeface.createFromAsset(getAssets(), "font/iransans.ttf");
            title.setTypeface(font);
            content.setTypeface(font);

            int color = getIntent().getIntExtra(InboxActivity.STATUSBAR_COLOR, -1);
            if (color != -1) {
                changeStatusbarColor(color);
            }

            Display display = getWindowManager().getDefaultDisplay();
            final int screenWidth = display.getWidth();
//            int screenHeight = display.getHeight();

//            Async async = new Async(this, image, message.getimage(), screenWidth);
//            async.execute();

            if (message.getOpen() == 0) {
                message.updateOpen(1);
                message.updateFlag(Message.FLAG_PENDING);

                if (EasyHTTP.haveInternet(this)) {
                    EasyHTTP.visit(message.getMsgId(), DataManager.getServerUrl() + "api/v1/visit/", new Handler.Callback() {
                        @Override
                        public boolean handleMessage(android.os.Message msg) {
                            try {
                                message.updateFlag(Message.FLAG_OK);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            return true;
                        }
                    });
                }
            }


            final String imageUrl = DataManager.getServerUrl() + message.getimage();
            Glide.with(this)
                    .load(imageUrl)
                    .asBitmap()
                    .into(new BitmapImageViewTarget(imageView) {
                        @Override
                        protected void setResource(Bitmap image) {
//                            super.setResource(image);
                            int size = (screenWidth * image.getHeight()) / image.getWidth();

                            Log.d(Constant.TAG, "imageW: " + screenWidth + " imageH: " + size);
                            Glide.with(InboxDetailActivity.this)
                                    .load(imageUrl)
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .override(screenWidth, size)
                                    .into(imageView);

//                            Runnable runnable = new Runnable() {
//                                public void run() {
//                                    Toast.makeText(InboxDetailActivity.this, "finish", Toast.LENGTH_SHORT).show();
//                                }
//                            };
//                            new Handler().postDelayed(runnable, 200);
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    private void loadImage(final ImageView imageView, final Message message) {
//        Glide.with(this)
//                .load(DataManager.getServerUrl() + message.getimage())
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .into(image);
//    }

    private class Async extends AsyncTask<Void, Void, Integer> {

        private Context context;
        private ImageView imageView;
        private String imageUrl;
        private int screenWidth;

        public Async(Context context, ImageView imageView, String imageUrl, int screenWidth) {
            this.context = context;
            this.imageView = imageView;
            this.imageUrl = imageUrl;
            this.screenWidth = screenWidth;
        }

        @Override
        protected Integer doInBackground(Void... params) {
            int size = 0;
            try {
                Bitmap image = Picasso.with(context)
                        .load(DataManager.getServerUrl() + imageUrl)
                        .get();

                size = (screenWidth * image.getHeight()) / image.getWidth();

            } catch (IOException e) {
                e.printStackTrace();
            }

            return size;
        }

        @Override
        protected void onPostExecute(final Integer size) {
            Log.d(Constant.TAG, "imageW: " + screenWidth + " imageH: " + size);

            Glide.with(InboxDetailActivity.this)
                    .load(DataManager.getServerUrl() + imageUrl)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .override(screenWidth, size)
                    .into(imageView);

//            Picasso.with(InboxDetailActivity.this)
//                    .load(DataManager.getServerUrl() + imageUrl)
////                    .resize(screenWidth, size)
//                    .networkPolicy(NetworkPolicy.OFFLINE)
//                    .priority(Picasso.Priority.HIGH)
//                    .into(imageView, new Callback() {
//                        @Override
//                        public void onSuccess() {
//                            Log.d(Constant.TAG, "cache success");
//                        }
//
//                        @Override
//                        public void onError() {
//                            Log.d(Constant.TAG, "cache error");
//                            //Try again online if cache failed!
//                            Picasso.with(InboxDetailActivity.this)
//                                    .load(DataManager.getServerUrl() + imageUrl)
//                                    .resize(screenWidth, size)
//                                    .priority(Picasso.Priority.HIGH)
//                                    .into(imageView, new Callback() {
//                                        @Override
//                                        public void onSuccess() {
//                                            Log.d(Constant.TAG, "online success");
//                                        }
//
//                                        @Override
//                                        public void onError() {
//                                            Log.d(Constant.TAG, "online error");
//                                        }
//                                    });
//                        }
//                    });
        }
    }
}
