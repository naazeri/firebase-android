package ir.partsilicon.inbox;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

/***
 * Created by reza nazeri on 95/12/16.
 ***/
public class InboxListAdapter extends RecyclerView.Adapter<InboxListAdapter.ViewHolder> {

    //    private View.OnClickListener clickListener;
    private List<?> list;
    private int rowLayout;

    public InboxListAdapter(int rowLayout) {
//        this.clickListener = clickListener;
        this.rowLayout = rowLayout;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
//        view.setOnClickListener(clickListener);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Message data = (Message) list.get(position);
        holder.title.setText(data.getTitle());
        if (data.getOpen() == 1) {
            holder.title.setTypeface(holder.title.getTypeface(), Typeface.NORMAL);
        } else {
            holder.title.setTypeface(holder.title.getTypeface(), Typeface.BOLD);
        }
        holder.content.setText(Html.fromHtml(data.getContent()));
//        holder.content.setMovementMethod(LinkMovementMethod.getInstance());
        String visit = String.valueOf(data.getVisit());
        visit = convertToPersianNumber(visit);
        holder.visit.setText(visit);

        try {
            Glide.with(holder.context)
                    .load(DataManager.getServerUrl() + data.getimage())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .fitCenter()
//                    .centerCrop()
                    .into(holder.image);

//            Picasso.with(holder.image.getContext())
//                    .load(DataManager.getServerUrl() + data.getimage())
//                    .resize(0, 300)
////                    .centerInside()
//                    .priority(Picasso.Priority.HIGH)
//                    .into(holder.image);
        } catch (Exception e) {
            e.printStackTrace();
        }

//        Glide.with(holder.image.getContext())
//                .load(DataManager.getServerUrl() + data.getimage())
//                .fitCenter()
//                .centerCrop()
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .into(holder.image);

        try {
            Category category = Category.getWithId(data.getCategory());
            if (category != null) {
                Picasso.with(holder.catIcon.getContext())
                        .load(DataManager.getServerUrl() + category.getimage())
                        .priority(Picasso.Priority.HIGH)
                        .into(holder.catIcon);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            Locale loc = new Locale("en_US");
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm", loc);
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dateFormat.parse(data.getCreatedAt()));
            CalendarTool calendarTool = new CalendarTool(calendar.getTime());

            // change timezone
            dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Tehran"));
            calendar.setTime(dateFormat.parse(dateFormat.format(calendar.getTime())));
            String persianTime = String.format(loc, "%s %02d:%02d",
                    calendarTool.getShamsiDate(),
                    calendar.get(Calendar.HOUR_OF_DAY),
                    calendar.get(Calendar.MINUTE));
            persianTime = convertToPersianNumber(persianTime);
            holder.time.setText(persianTime);
        } catch (ParseException e) {
            e.printStackTrace();
            Log.d(Constant.TAG, "parse error: ", e);
            holder.time.setText("");
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setData(List<?> list) {
        this.list = (list != null) ? list : new LinkedList<>();
        notifyDataSetChanged();
    }

    public List<?> getData() {
        return list;
    }

    private String convertToPersianNumber(String s) {
        int length = s.length();
        StringBuilder str = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            char n = s.charAt(i);
            switch (n) {
                case '0':
                    str.append('۰');
                    break;
                case '1':
                    str.append('۱');
                    break;
                case '2':
                    str.append('۲');
                    break;
                case '3':
                    str.append('۳');
                    break;
                case '4':
                    str.append('۴');
                    break;
                case '5':
                    str.append('۵');
                    break;
                case '6':
                    str.append('۶');
                    break;
                case '7':
                    str.append('۷');
                    break;
                case '8':
                    str.append('۸');
                    break;
                case '9':
                    str.append('۹');
                    break;
                default:
                    str.append(n);
                    break;
            }
        }
        return str.toString();
    }

//    public void setData(List<Zekr> list) {
//        this.list = list;
//        notifyDataSetChanged();
//    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public Context context;
        public TextView title, content, time, visit;
        public ImageView image, share, catIcon;
        public CardView cardView;

        public ViewHolder(View view) {
            super(view);
            context = view.getContext();
            title = (TextView) view.findViewById(R.id.list_title);
            content = (TextView) view.findViewById(R.id.list_content);
            time = (TextView) view.findViewById(R.id.list_time);
            visit = (TextView) view.findViewById(R.id.list_visit);
            image = (ImageView) view.findViewById(R.id.list_image);
            share = (ImageView) view.findViewById(R.id.list_share);
            catIcon = (ImageView) view.findViewById(R.id.list_cat_icon);
            cardView = (CardView) view.findViewById(R.id.list_cardview);

            Typeface font = Typeface.createFromAsset(context.getAssets(), "font/iransans.ttf");
            title.setTypeface(font);
            content.setTypeface(font);
            time.setTypeface(font);
            visit.setTypeface(font);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    InboxActivity activity = (InboxActivity) context;
                    activity.onItemClickListener(getAdapterPosition());
                }
            });

            share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    InboxActivity activity = (InboxActivity) context;
                    activity.onShareItemClick(getAdapterPosition());
                }
            });
        }
    }
}
