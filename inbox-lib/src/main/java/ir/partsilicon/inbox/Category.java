package ir.partsilicon.inbox;

import android.os.Parcel;
import android.os.Parcelable;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/***
 * Created by reza on 7/2/17.
 ***/

@Table(name = "Category", id = "_id")
public class Category extends Model implements Parcelable {

    //@SerializedName("id")
    @Column(name = "id")
    public long catId;

    //@SerializedName("title")
    @Column(name = "title")
    public String title;

    //@SerializedName("image")
    @Column(name = "image")
    public String image;

    public Category() {
        super();
    }

    public Category(long catId, String title, String image) {
        super();
        this.catId = catId;
        this.title = title;
        this.image = image;
    }

    public Category(JSONObject json) {
        super();
        try {
            if (json.has("id")) {
                this.catId = json.getLong("id");
            }
            if (json.has("title")) {
                this.title = json.getString("title");
            }
            if (json.has("image")) {
                this.image = json.getString("image");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static List<Category> getAll() {
        List<Category> list = null;
        try {
            list = new Select()
                    .from(Category.class)
                    .orderBy("id")
                    .execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public static Category getWithId(long catId) {
        Category category = null;
        try {
            category = new Select()
                    .from(Category.class)
                    .where("id = ?", catId)
                    .executeSingle();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return category;
    }

    public long getCatId() {
        return catId;
    }

    public void setCatId(long catId) {
        this.catId = catId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getimage() {
        return image;
    }

    public void setimage(String image) {
        this.image = image;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.catId);
        dest.writeString(this.title);
        dest.writeString(this.image);
    }

    protected Category(Parcel in) {
        this.catId = in.readLong();
        this.title = in.readString();
        this.image = in.readString();
    }

    public static final Parcelable.Creator<Category> CREATOR = new Parcelable.Creator<Category>() {
        @Override
        public Category createFromParcel(Parcel source) {
            return new Category(source);
        }

        @Override
        public Category[] newArray(int size) {
            return new Category[size];
        }
    };

}
