package ir.partsilicon.inbox;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;

import java.util.LinkedList;
import java.util.List;

public class InboxActivity extends BaseActivity {

    public static final String STATUSBAR_COLOR = "statusColor";
    private RecyclerView recyclerView, navRecyclerView;
    private List<Message> list;
    private List<Category> navList;
    private DrawerLayout drawer;
    private int color;
    private View emptyTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inbox);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        try {
            emptyTextView = findViewById(R.id.inbox_empty);
            drawer = (DrawerLayout) findViewById(R.id.inbox_drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.setDrawerListener(toggle);
            toggle.syncState();

            recyclerView = (RecyclerView) findViewById(R.id.inbox_recycler);
            navRecyclerView = (RecyclerView) findViewById(R.id.inbox_nav_recycler);

            initRecycler(this, recyclerView, R.layout.inbox_list_item, false);
            initRecyclerNav(this, navRecyclerView, R.layout.nav_list_item, true);

            color = getIntent().getIntExtra(STATUSBAR_COLOR, -1);
            if (color != -1) {
                changeStatusbarColor(color);
            }

//        list = DataManager.getInboxList();
//        navList = DataManager.getDataFromServer();
//        updateRecycler(recyclerView, list);
//        updateRecyclerNav(navRecyclerView, navList);

//        try {
//            String d = "2017-07 -2T00:25:17.088Z";
//            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
//            Calendar c = Calendar.getInstance();
//            c.setTime(dateFormat.parse(d));
//
//            CalendarTool s = new CalendarTool(c.getTime());
////            s.setJulianDate(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
//            Log.d(Constant.TAG, "shamsi date: " + s.getShamsiDate());
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initRecycler(Context context, RecyclerView recyclerView, int rowLayout, boolean haveDivider) {
        if (recyclerView != null) {
            recyclerView.setHasFixedSize(false);
            LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(layoutManager);
            InboxListAdapter adapter = new InboxListAdapter(rowLayout);
//            adapter.setHasStableIds(true);
            recyclerView.setAdapter(adapter);
//            recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(context, onItemClickListener));
            if (haveDivider) {
                /// divider
                recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), layoutManager.getOrientation()));
            }
        }
    }

    private void initRecyclerNav(Context context, RecyclerView recyclerView, int rowLayout, boolean haveDivider) {
        if (recyclerView != null) {
            recyclerView.setHasFixedSize(false);
            LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(layoutManager);
            NavListAdapter adapter = new NavListAdapter(rowLayout);
//            adapter.setHasStableIds(true);
            recyclerView.setAdapter(adapter);
//            recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(context, onItemClickListener));
            if (haveDivider) {
                /// divider
                recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), layoutManager.getOrientation()));
            }
        }
    }

    private void updateRecycler(RecyclerView recyclerView, List<?> list) {
        if (recyclerView != null) {
            InboxListAdapter adapter = (InboxListAdapter) recyclerView.getAdapter();
            adapter.setData(list);
        }
    }

    private void updateRecyclerNav(RecyclerView recyclerView, List<?> list) {
        if (recyclerView != null) {
            NavListAdapter adapter = (NavListAdapter) recyclerView.getAdapter();
            adapter.setData(list);
        }
    }

    public void onItemClickListener(int position) {
        Intent intent = new Intent(InboxActivity.this, InboxDetailActivity.class);
        InboxListAdapter adapter = (InboxListAdapter) recyclerView.getAdapter();
        List<?> list = adapter.getData();
        Message message = (Message) list.get(position);
        intent.putExtra(InboxDetailActivity.MESSAGE, message);
        intent.putExtra(STATUSBAR_COLOR, color);
        startActivity(intent);
    }

    public void onNavItemClickListener(int position) {
        if (drawer.isDrawerOpen(Gravity.RIGHT)) {
            drawer.closeDrawer(Gravity.RIGHT);
        } else {
            drawer.openDrawer(Gravity.RIGHT);
        }

        if (position == 0) {
            updateRecycler(recyclerView, list);
        } else {
            NavListAdapter adapter = (NavListAdapter) navRecyclerView.getAdapter();
            Category category = (Category) adapter.getData().get(position);
            List<Message> newList = new LinkedList<>();
            for (Message message : list) {
                if (message.getCategory() == category.getCatId()) {
                    newList.add(message);
                }
            }
            updateRecycler(recyclerView, newList);
        }
    }

    public void onShareItemClick(int position) {
        Message message = list.get(position);

        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, message.getTitle());
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, message.getTitle() + "\n\n" + message.getContent());
        startActivity(Intent.createChooser(sharingIntent, "اشتراک گذاری"));
    }

//    @Override
//    protected void onStart() {
//        super.onStart();
//        List<Message> message = EasyHTTP.getMessages("http://192.168.1.124:3000/api/v1/message/0");
//        DataManager.saveMessage(message);
//    }

    @Override
    protected void onResume() {
        try {
            super.onResume();
            updateUI();
            updateVisit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateUI() {
        Log.d(Constant.TAG, "unread message: " + Message.getUnreadCount());

        if (list != null && !list.isEmpty()) {
            list.clear();
        }
        if (navList != null && !navList.isEmpty()) {
            navList.clear();
        }

        list = Message.getAll();
        navList = new LinkedList<>();
        List<Category> categoryList = Category.getAll();
        if (categoryList != null && !categoryList.isEmpty()) {
            navList.add(new Category(0, "همه", ""));
            navList.addAll(categoryList);
        } else {
            navList.add(new Category(0, "دسته بندی موجود نیست", ""));
            if (DataManager.isFirstOpen()) {
                getDataFromServer();
            }
        }

        if (list != null && list.size() > 0) {
            emptyTextView.setVisibility(View.GONE);
        } else {
            emptyTextView.setVisibility(View.VISIBLE);
        }

        updateRecycler(recyclerView, list);
        updateRecyclerNav(navRecyclerView, navList);
        Log.d(Constant.TAG, "message list size: " + list.size());
        Log.d(Constant.TAG, "nav list size: " + navList.size());
    }

    private void updateVisit() {
        final String serverUrl = DataManager.getServerUrl();
        if (serverUrl.isEmpty()) {
            return;
        }

        Runnable runnable = new Runnable() {
            public void run() {
                for (final Message message : list) {
                    if (message.getFlag() == Message.FLAG_PENDING) {
                        EasyHTTP.visit(message.getMsgId(), serverUrl + "api/v1/visit/", new Handler.Callback() {
                            @Override
                            public boolean handleMessage(android.os.Message msg) {
                                message.updateFlag(Message.FLAG_OK);
                                return true;
                            }
                        });
                    }
                }

                boolean isSuccess = EasyHTTP.getVisit(serverUrl + "api/v1/visit/-1", DataManager.getPackageName());
                if (isSuccess) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            Log.d(Constant.TAG, "get visit end");
                            updateUI();
                        }
                    });
                }
            }
        };

        Thread thread = new Thread(runnable);
        thread.setPriority(Thread.NORM_PRIORITY);
        thread.start();
    }

    private void getDataFromServer() {
        // get server url from firebase remote config
        final FirebaseRemoteConfig config = FirebaseRemoteConfig.getInstance();
        config.fetch(60).addOnCompleteListener(InboxActivity.this, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    config.activateFetched();

//                    final String serverUrl = "http://192.168.1.124:3000/";
                    final String serverUrl = config.getString("server_url");
                    final String catMethod = config.getString("cat_method");
                    final String messageMethod = config.getString("message_method");

                    if (serverUrl != null && !serverUrl.isEmpty()) {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                List<Category> list = EasyHTTP.getCategory(serverUrl + catMethod, DataManager.getPackageName());
                                List<Message> messages = EasyHTTP.getMessages(serverUrl + messageMethod + 0, DataManager.getPackageName());
                                DataManager.saveServerUrl(serverUrl);
                                DataManager.saveCategory(list);
                                DataManager.saveMessage(messages);
                                DataManager.setFirstOpen(false);
                                Log.d(Constant.TAG, "message and category updated");

                                new Handler(Looper.getMainLooper()).post(new Runnable() {
                                    @Override
                                    public void run() {
                                        updateUI();
                                    }
                                });
                            }
                        }).start();
                    } else {
                        Log.d(Constant.TAG, "can't get server url for get data");
                    }

                } else {
                    Log.d(Constant.TAG, "config task did not successful");
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.inbox_drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_inbox, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.menu_right) {
            if (drawer.isDrawerOpen(Gravity.RIGHT)) {
                drawer.closeDrawer(Gravity.RIGHT);
            } else {
                drawer.openDrawer(Gravity.RIGHT);
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
