package ir.partsilicon.inbox;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.os.Handler;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessaging;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

/***
 * Created by reza on 6/13/17.
 ***/

public class InboxManager {

    public static void init(Context context) {
        DataManager.init(context);
        DataManager.savePackageName(context);
        subscribe("t");
//        Log.d(Constant.TAG, "subscribe on: " + applicationName);
        checkForPopup(context);
    }

    public static int getUnreadMessageCount() {
        return Message.getUnreadCount();
    }

    private static void subscribe(String s) {
        FirebaseMessaging.getInstance().subscribeToTopic(s);
    }

    private static void checkForPopup(final Context context) {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                final Message message = Message.getLastPopup();
                if (message != null) {
                    String url = DataManager.getServerUrl() + message.getimage();
                    Picasso.with(context).load(url)
                            .noFade()
                            .fetch(new Callback() {
                                @Override
                                public void onSuccess() {
                                    Log.d(Constant.TAG, "show popup with id: " + message.getMsgId());
                                    Intent intent = new Intent(context, PopupActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    intent.putExtra("message", message);
                                    context.startActivity(intent);
                                    message.setOpenLastPopup();
                                    message.updateOpen(1);
                                }

                                @Override
                                public void onError() {
                                    Log.d(Constant.TAG, "error on caching popup image");
                                }
                            });
                }
            }
        }, 3 * 1000);
    }

//    private static String getApplicationName(Context context) {
//        ApplicationInfo applicationInfo = context.getApplicationInfo();
//        int stringId = applicationInfo.labelRes;
//        return stringId == 0 ? applicationInfo.nonLocalizedLabel.toString() : context.getString(stringId);
//    }
}
