package ir.partsilicon.app;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.LinkedList;
import java.util.List;

/***
 * Created by reza nazeri on 95/12/16.
 ***/
class FooListAdapter extends RecyclerView.Adapter<FooListAdapter.ViewHolder> {

    //    private View.OnClickListener clickListener;
    private List<?> list = new LinkedList<>();
    private int rowLayout;

    public FooListAdapter(int rowLayout) {
//        this.clickListener = clickListener;
        this.rowLayout = rowLayout;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
//        view.setOnClickListener(clickListener);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String data = (String) list.get(position);
        holder.title.setText(data);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setData(List<?> list) {
        this.list = list != null ? list : new LinkedList<>();
        notifyDataSetChanged();
    }

    public List<?> getData() {
        return list;
    }

//    public void setData(List<Zekr> list) {
//        this.list = list;
//        notifyDataSetChanged();
//    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public Context context;
        public TextView title;
        public ViewHolder(View view) {
            super(view);
            context = view.getContext();
            title = (TextView) view.findViewById(R.id.foo_list_textView);

//            view.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    InboxActivity activity = (InboxActivity) context;
//                    activity.onItemClickListener(getAdapterPosition());
//                }
//            });
        }
    }
}
